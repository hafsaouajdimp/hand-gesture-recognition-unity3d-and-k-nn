using UnityEngine;

public class SystemEvents : MonoBehaviour
{
    public Light mainLight;
    public Transform environment;

    public void TurnOffLight()
    {
        mainLight.enabled = false;
    }

    public void TurnOnLight()
    {
        mainLight.enabled = true;
    }

    public void ScaleDown()
    {
        environment.localScale = Vector3.Max(0.5f * Vector3.one, environment.localScale - 0.1f * Vector3.one);
    }

    public void ScaleUp()
    {
        environment.localScale = Vector3.Min(1.5f * Vector3.one, environment.localScale + 0.1f * Vector3.one);
    }
}
