﻿using System.Collections.Generic;
using UnityEngine;

namespace TP5
{
	public class Classifier : MonoBehaviour
	{
		public class Prediction
		{
			public float distance;
			public float probability;

			public Prediction(float distance)
			{
				this.distance = distance;

				probability = 0;
			}
		}

		public class Frame
		{
			public Vector3 thumb;
			public Vector3 index;
			public Vector3 middle;
			public Vector3 ring;
			public Vector3 pinky;
		}

		public class Gesture
		{
			public string name;
			public List<Frame> frames;

			public Gesture(int index)
			{
				name = string.Format("Gesture {0}", index + 1);

				frames = new List<Frame>();
			}
		}

		#region Members
		public const byte nbFingers = 5;

		public HandUtils hand;
		[Range(0.001f, 0.2f)]
		public float captureFrequency = 0.02f; // In seconds
		[Range(0.5f, 10f)]
		public float predictionWindowSize = 1.5f; // In seconds
		[Range(0.01f, 10f)]
		public float nullRejectionFactor = 2f;
		[Range(0.01f, 10f)]
		public float softmaxFactor = 1f;

		public delegate void UpdateCallback(Frame frame);

		public event UpdateCallback UpdateCallbacks;

		protected List<Gesture> gestures;
		
		protected List<Gesture> gesturesCentres;
		protected Queue<Frame> window;
		protected List<Frame> frames;
		protected List<Prediction> predictions;
		protected Algo.NullRejection nullRejection;
		protected Transform palm;
		#endregion

		#region Getters / Setters
		public List<Gesture> Gestures
		{
			get { return gestures; }
		}

		public Transform Palm
		{
			get { return palm; }
		}
		#endregion

		#region MonoBehaviour callbacks
		protected void Awake()
		{
			Time.fixedDeltaTime = captureFrequency;

			gestures = new List<Gesture>();
			window = new Queue<Frame>(GetPredictionWindowSize());
			frames = new List<Frame>(GetPredictionWindowSize());
			predictions = new List<Prediction>();
			nullRejection = new Algo.NullRejection();
		}

		protected void Update()
		{
			if(Time.fixedDeltaTime != captureFrequency)
			{
				Time.fixedDeltaTime = captureFrequency;
			}
		}

		protected void FixedUpdate()
		{
			if (hand?.Wrist != null)
			{
				Frame frame = new Frame();

				for (byte i = 0; i < nbFingers; ++i)
				{
                    frame.thumb = hand.Wrist.InverseTransformPoint(hand.Thumb.position);
                    frame.index = hand.Wrist.InverseTransformPoint(hand.Index.position);
                    frame.middle = hand.Wrist.InverseTransformPoint(hand.Middle.position);
                    frame.ring = hand.Wrist.InverseTransformPoint(hand.Ring.position);
                    frame.pinky = hand.Wrist.InverseTransformPoint(hand.Pinky.position);
                }

				lock (window)
				{
					while (window.Count >= GetPredictionWindowSize())
					{
						window.Dequeue();
					}

					window.Enqueue(frame);
				}

				UpdateCallbacks.Invoke(frame);
			}
		}
        #endregion

        #region Public methods
        public void Train()
        {
            nullRejection.Train(gestures);
        }
        //public void Train()
        //{
        //	nullRejection.Train(gesturesCentres);
        //}

        public List<Prediction> Predict()
        {
            frames.Clear();
            predictions.Clear();

            lock (window)
            {
                foreach (Frame frame in window)
                {
                    frames.Add(frame);
                }
            }

            int nb_gestures = gestures.Count;

            for (int i = 0; i < nb_gestures; ++i)
            {
                predictions.Add(new Prediction(Algo.DTW.Distance(gestures[i].frames, frames)));
            }

            nullRejection.AddNullClass(predictions, nullRejectionFactor);

            Algo.Softmax.Normalize(predictions, softmaxFactor);

            return predictions;
        }

  //      //for centres 
  //      public List<Prediction> Predict()
		//{
		//	frames.Clear();
		//	predictions.Clear();
			


		//	lock (window)
		//	{
		//		foreach (Frame frame in window)
		//		{
		//			frames.Add(frame);
		//		}
		//	}

		//	int nb_gestures = gesturesCentres.Count;

		//	for (int i = 0; i < nb_gestures; ++i)
		//	{
		//		gesturesCentres[i] = Algo.DTW.GetThecenter(gestures[i]);
		//		predictions.Add(new Prediction(Algo.DTW.Distance(gesturesCentres[i].frames, frames)));
		//	}

		//	nullRejection.AddNullClass(predictions, nullRejectionFactor);

		//	Algo.Softmax.Normalize(predictions, softmaxFactor);

		//	return predictions;
		//}
		#endregion

		#region Internal methods
		protected int GetPredictionWindowSize()
		{
			return (int) (predictionWindowSize / captureFrequency);
		}
		#endregion
	}
}
