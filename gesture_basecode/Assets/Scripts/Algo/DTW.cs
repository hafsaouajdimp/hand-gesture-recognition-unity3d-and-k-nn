﻿using System.Collections.Generic;
using UnityEngine;

namespace TP5.Algo
{
	public class DTW
	{
		public static float Distance(IList<Classifier.Frame> gesture, IList<Classifier.Frame> data)
		{
			float shortest_distance = float.MaxValue;


			if(gesture != null && data != null)
			{
				// TODO
				int n = gesture.Count;
				int m = data.Count;
				if(m>0 && n > 0)
                {
					float[,] distances = new float[n, m];
					for (int i = 0; i < n; i++)
                    {
						distances[i, 0]= FramesDistance(gesture[i], data[0]);

					}
					for (int j = 0; j < m; j++)
					{
						distances[0, j] = FramesDistance(gesture[0], data[j]);

					}

					for (int i = 1; i < n; i++)
					{
						for (int j = 1; j < m; j++)
						{
							float cost = FramesDistance(gesture[i], data[j]);
							distances[i, j] = cost + Mathf.Min(distances[i, j - 1], distances[i - 1, j], distances[i - 1, j - 1]);
						}
					}
					shortest_distance = distances[n-1, m-1];
				}
			}
				
			
			return shortest_distance;
		}
		public static IList<Classifier.Frame> GetThecenter(IList<IList<Classifier.Frame>> gestures)//gestures belong to the same category
		{
			

			if (gestures != null)
            {
				int n = gestures.Count;
				int indice = 0;
				if (n > 0)
                {
					float distanceMin=float.MaxValue;
					
					for (int i = 0; i < n; i++)
                    {
						float curentDistance = 0;

						for (int j = 0; j < n; j++)
                        {
							curentDistance += Distance(gestures[i], gestures[j]);
							

						}
                        if (curentDistance < distanceMin)
                        {
							indice = i;
							distanceMin = curentDistance;
                        }
						
					}
					return gestures[indice];

				}

			}
			return null;

			
			
		}

		protected static float FramesDistance(Classifier.Frame f1, Classifier.Frame f2)
		{
			// TODO
			

			float distance;
			distance = (f1.thumb - f2.thumb).magnitude+(f1.index - f2.index).magnitude+(f1.middle - f2.middle).magnitude+(f1.ring - f2.ring).magnitude+(f1.pinky - f2.pinky).magnitude;

			return distance/5;
			//return float.MaxValue;
		}
	}
}
