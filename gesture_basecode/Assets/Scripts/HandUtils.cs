﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(OVRSkeleton))]
public class HandUtils : MonoBehaviour {

    public Transform Wrist { get; protected set; }
    public Transform Thumb { get; protected set; }
    public Transform Index { get; protected set; }
    public Transform Middle { get; protected set; }
    public Transform Ring { get; protected set; }
    public Transform Pinky { get; protected set; }

    protected void Awake() {
        StartCoroutine(InitHand(GetComponent<OVRSkeleton>()));
	}

	protected IEnumerator InitHand(OVRSkeleton skeleton)
	{
		while(!skeleton.IsInitialized)
		{
			yield return null;
		}

        foreach (OVRBone bone in skeleton.Bones)
        {
            switch (bone.Id)
            {
                case OVRSkeleton.BoneId.Hand_WristRoot:
                    Wrist = bone.Transform;

                    break;
                case OVRSkeleton.BoneId.Hand_ThumbTip:
                    Thumb = bone.Transform;

                    break;
                case OVRSkeleton.BoneId.Hand_IndexTip:
					Index = bone.Transform;

                    break;
                case OVRSkeleton.BoneId.Hand_MiddleTip:
					Middle = bone.Transform;

                    break;
                case OVRSkeleton.BoneId.Hand_RingTip:
                    Ring = bone.Transform;

                    break;
                case OVRSkeleton.BoneId.Hand_PinkyTip:
                    Pinky = bone.Transform;

                    break;
            }
        }
    }
}
