﻿using System.Collections.Generic;
using UnityEngine;

namespace TP5.Algo
{
	public class Softmax
	{
		public static void Normalize(List<Classifier.Prediction> data, float b = 1)
		{
			if(data != null)
			{
				// TODO
				int n = data.Count;
				float sum= 0;
				for (int i = 0; i < n; i++)
				{
					sum += Mathf.Exp(-b * data[i].distance);
				}

				for (int i = 0; i < n; i++)
                {
					data[i].probability = Mathf.Exp(-b * data[i].distance)/sum;
                }
			}
		}
	}
}
