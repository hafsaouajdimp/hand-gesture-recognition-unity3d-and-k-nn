﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TP5.Algo
{
	public class NullRejection
	{
		#region Members
		protected List<float> interDistances = new List<float>();
		protected float threshold;
		#endregion
		#region Public methods
		public void Train(IList<Classifier.Gesture> gestures)
		{
			
			// TODO
			int n = gestures.Count;
            if (n >1)
            {
				float distanceMin = DTW.Distance(gestures[0].frames, gestures[1].frames);
				for (int i = 0; i < n; i++)
				{
					for (int j = i; j < n; j++)
					{

						float dist = DTW.Distance(gestures[i].frames, gestures[j].frames);
						interDistances.Add(dist);
						if (dist < distanceMin)
						{
							distanceMin = dist;
						}
					}
				}
				threshold = distanceMin / 2;
			}
			
		}

		public void AddNullClass(List<Classifier.Prediction> data, float factor)
		{
			float distNull;
			int m = data.Count;
			if (m > 0)
			{
				// TODO
				distNull = factor * Mathf.Abs(threshold - data[0].distance);

				int n = data.Count;
				for(int i = 0; i < n; i++)
                {
					float dis = factor * Mathf.Abs(threshold - data[i].distance);
					if (distNull > dis)
					{
						distNull = dis;
					}

				}
				Classifier.Prediction prediction = new Classifier.Prediction(distNull);
				data.Add(prediction);

			}



		}
		#endregion
	}
}
